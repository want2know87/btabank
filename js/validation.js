$(function () {
    var errorClass = 'form-error';
    var $checkbox  = $('#check');
    var $button = $('.page-form__btn');

    $(".page-form__input--tel").inputmask("+375 (99) 999-99-99");

    $button.attr('disabled', $(this).is(':checked'));

    function displayRequiredError($element, callback) {
        if (callback($element)) {
            $element.parent().addClass(errorClass);
            return true;
        } else {
            $element.parent().removeClass(errorClass);
            return false;
        }
    }

    $('#form input').focus(function () {
        $(this).parent().removeClass(errorClass);
    });

    $checkbox.change(function () {
        $button.attr('disabled', !$(this).is(':checked'));
    });

    $('#form').submit(function (e) {
        var $textInput = $('#form_text'),
            $telInput  = $('#form_tel');
        var textInputError = displayRequiredError($textInput, function ($el) {
            return !$el.val().trim();
        });
        var telInputError = displayRequiredError($telInput, function ($el) {
            return !$el.inputmask("isComplete");
        });
        if (textInputError || telInputError) {
            e.preventDefault();
        }
    });
});

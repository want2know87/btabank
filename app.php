<?php

header("Content-Type: text/html;charset=utf-8");

$params = require_once 'params.php';

$bankEmail = $params['bankEmail'];

if (isset($_POST['text']) && !empty($_POST['text'])
 && isset($_POST['tel']) && !empty($_POST['tel'])
) {
	$name = stripslashes(trim(htmlspecialchars($_POST['text'])));
	$tel = stripslashes(trim(htmlspecialchars($_POST['tel'])));
    $date = date('d.m.Y H:i');
	$message = <<<TXT
Заявка на кредит.
Дата: $date
Имя: $name
Номер: $tel
TXT;
	mail($bankEmail, 'BTA Bank. Заявка на кредит', $message);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>BTA</title>
    <link rel="stylesheet" href="css/style2.css">
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/modernizr-custom.js"></script>
    <script src="js/svg4everybody.js"></script>
    <script>
    svg4everybody();
    </script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <!-- Header -->
    <header class="page-header">
        <a href="#" class="logo">
            <img src="assets/img/logo/logo.png" alt="БТА Банк">
        </a>
    </header>
    <!-- End of Header -->
    <!-- Main -->
    <main>
        <div class="page-main page-main--no-bg">
            <h1 class="page-main__title page-main__title--left">
            Спасибо за <span>заявку</span>
            </h1>
            <div class="page-main__tel">
                5-282-282
            </div>
            <p class="page-main__desc page-main__desc--full">
                Финансовый консультант свяжется с Вами в ближайшее время и ответит на интересующие вопросы.
            </p>
        </div>
    </main>
    <!-- End of Main -->
    <script src="js/jquery-3.1.1.min.js"></script>
</body>
</html>
<?php } ?>
